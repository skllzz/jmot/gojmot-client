package metrics

import (
	"context"
	"fmt"
	"sync/atomic"
)

var statsContext = "statsContext"

type OpStats struct {
	reads   uint64
	writes  uint64
	deletes uint64
}

func (ss *OpStats) IncWrites() {
	if ss == nil {
		return
	}
	atomic.AddUint64(&ss.writes, 1)
}
func (ss *OpStats) AddWrites(delta uint64) {
	if ss == nil {
		return
	}
	atomic.AddUint64(&ss.writes, delta)
}

func (ss *OpStats) IncReads() {
	if ss == nil {
		return
	}
	atomic.AddUint64(&ss.reads, 1)
}

func (ss *OpStats) Reads() uint64 {
	if ss == nil {
		return 0
	}
	return atomic.LoadUint64(&ss.reads)
}

func (ss *OpStats) Writes() uint64 {
	if ss == nil {
		return 0
	}
	return atomic.LoadUint64(&ss.writes)
}

func (ss *OpStats) Deletes() uint64 {
	if ss == nil {
		return 0
	}
	return atomic.LoadUint64(&ss.deletes)
}

func (ss *OpStats) IncDeletes() {
	if ss == nil {
		return
	}
	atomic.AddUint64(&ss.deletes, 1)
}

func (ss *OpStats) Reset() {
	if ss == nil {
		return
	}
	atomic.StoreUint64(&ss.writes, 0)
	atomic.StoreUint64(&ss.reads, 0)
	atomic.StoreUint64(&ss.deletes, 0)
}

func (ss *OpStats) String() string {
	return fmt.Sprintf("reads:%v writes:%v deletes:%v",
		ss.Reads(),
		ss.Writes(),
		ss.Deletes(),
	)
}

func ContextStats(ctx context.Context) *OpStats {
	if stats, ok := ctx.Value(statsContext).(*OpStats); ok {
		return stats
	}
	return nil
}
func ContextWithStats(ctx context.Context) context.Context {
	if _, ok := ctx.Value(statsContext).(*OpStats); ok {
		return ctx
	}
	return context.WithValue(ctx, statsContext, &OpStats{})
}
