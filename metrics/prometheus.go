package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
)

var ApiCalls = prometheus.NewCounterVec(prometheus.CounterOpts{
	Name: prometheus.BuildFQName("jmot", "api", "calls"),
	Help: "API calls",
}, []string{"method"})

var ApiErrors = prometheus.NewCounterVec(prometheus.CounterOpts{
	Name: prometheus.BuildFQName("jmot", "api", "errors"),
	Help: "API call errors",
}, []string{"method"})

func init() {
	prometheus.MustRegister(ApiCalls)
	prometheus.MustRegister(ApiErrors)
}
