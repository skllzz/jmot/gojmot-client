package gojmot_client

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/binary"
	"errors"
	"fmt"
	"gitlab.com/skllzz/jmot/gojmot-client/logging"
	"gitlab.com/skllzz/jmot/gojmot-client/metrics"
	"gitlab.com/skllzz/jmot/gojmot-client/middleware"
	"gitlab.com/skllzz/jmot/gojmot-client/pb/keeper/arrays"
	"gitlab.com/skllzz/jmot/gojmot-client/pb/keeper/backup"
	"gitlab.com/skllzz/jmot/gojmot-client/pb/keeper/data"
	"gitlab.com/skllzz/jmot/gojmot-client/pb/keeper/journals"
	"gitlab.com/skllzz/jmot/gojmot-client/pb/keeper/queues"
	"gitlab.com/skllzz/jmot/gojmot-client/pb/keeper/tasks"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/protobuf/types/known/timestamppb"
	"io"
	"time"
)

var ErrNotConnected = fmt.Errorf("connection not initialized")

type Journals struct {
	journals.OperatorClient
}

func (j *Journals) Read(ctx context.Context, in *journals.ReadRequest, opts ...grpc.CallOption) <-chan *Item {
	return reader(ctx).dataOrErrorChannel(j.OperatorClient.Read(ctx, in, opts...))
}

func (j *Journals) Append(ctx context.Context, in *journals.AppendRequest, opts ...grpc.CallOption) (*data.Item, error) {
	if j.OperatorClient == nil {
		return nil, ErrNotConnected
	}
	metrics.ContextStats(ctx).IncWrites()
	return j.OperatorClient.Append(ctx, in, opts...)
}

func (j *Journals) Value(ctx context.Context, in *journals.ValueRequest, opts ...grpc.CallOption) (*data.Item, error) {
	if j.OperatorClient == nil {
		return nil, ErrNotConnected
	}
	metrics.ContextStats(ctx).IncReads()
	return j.OperatorClient.Value(ctx, in, opts...)
}

type Arrays struct {
	arrays.OperatorClient
}

func (a *Arrays) Read(ctx context.Context, in *arrays.ReadRequest, opts ...grpc.CallOption) <-chan *Item {
	return reader(ctx).dataOrErrorChannel(a.OperatorClient.Read(ctx, in, opts...))
}

func (a *Arrays) Append(ctx context.Context, in *arrays.AppendRequest, opts ...grpc.CallOption) (*data.Item, error) {
	if a.OperatorClient == nil {
		return nil, ErrNotConnected
	}
	metrics.ContextStats(ctx).IncWrites()
	return a.OperatorClient.Append(ctx, in, opts...)
}

func (a *Arrays) AppendMultiple(ctx context.Context, in []*arrays.AppendRequest, opts ...grpc.CallOption) error {
	if a.OperatorClient == nil {
		return ErrNotConnected
	}
	metrics.ContextStats(ctx).AddWrites(uint64(len(in)))
	if _, err := a.OperatorClient.AppendMultiple(ctx, &arrays.AppendMultipleRequest{Ops: in}, opts...); err != nil {
		return err
	}
	return nil
}

func (a *Arrays) Value(ctx context.Context, in *arrays.ValueRequest, opts ...grpc.CallOption) (*data.Item, error) {
	if a.OperatorClient == nil {
		return nil, ErrNotConnected
	}
	metrics.ContextStats(ctx).IncReads()
	return a.OperatorClient.Value(ctx, in, opts...)
}

type Queues struct {
	queues.OperatorClient
}

func (q *Queues) Pull(ctx context.Context, in *queues.PullRequest, opts ...grpc.CallOption) <-chan *Item {
	return reader(ctx).dataOrErrorChannel(q.OperatorClient.Pull(ctx, in, opts...))
}

func (q *Queues) Push(ctx context.Context, in *queues.PushRequest, opts ...grpc.CallOption) (*data.Item, error) {
	if q.OperatorClient == nil {
		return nil, ErrNotConnected
	}
	metrics.ContextStats(ctx).IncWrites()
	return q.OperatorClient.Push(ctx, in, opts...)
}

func (q *Queues) Delete(ctx context.Context, in *queues.DeleteRequest, opts ...grpc.CallOption) (*data.Item, error) {
	if q.OperatorClient == nil {
		return nil, ErrNotConnected
	}
	if in.Item == nil {
		return nil, errors.New("item not defined")
	}
	metrics.ContextStats(ctx).IncDeletes()
	return q.OperatorClient.Delete(ctx,
		&queues.DeleteRequest{QueuesName: in.QueuesName, Item: &data.Item{
			Id:   in.Item.Id,
			Type: in.Item.Type,
		}}, opts...)
}

type Tasks struct {
	tasks.OperatorClient
}
type Backups struct {
	backup.BackupClient
}

func (bkp *Backups) ParseReader(ctx context.Context, reader io.Reader, forEach func(context.Context, *backup.BackupBlob) error) error {
	for ctx.Err() == nil {
		size := int32(0)
		if err := binary.Read(reader, binary.BigEndian, &size); err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		buf := make([]byte, size)
		if err := binary.Read(reader, binary.BigEndian, buf); err != nil {
			return err
		} else {
			blob := new(backup.BackupBlob)
			if err := Unmarshal(buf, blob); err != nil {
				return err
			} else {
				if err := forEach(ctx, blob); err != nil {
					return err
				}
			}
		}
	}
	return ctx.Err()
}
func (bkp *Backups) LoadFrom(ctx context.Context, source <-chan *backup.BackupBlob) error {
	if upload, err := bkp.BackupClient.Upload(ctx); err != nil {
		return err
	} else {
	done:
		for ctx.Err() == nil {
			select {
			case <-ctx.Done():
				break done
			case item := <-source:
				if item == nil {
					break done
				}
				if err := upload.Send(item); err != nil {
					return err
				}
			}
		}
		return upload.CloseSend()
	}
}
func (bkp *Backups) WriteTo(ctx context.Context, btype backup.BackupBlob_Type, writer io.Writer, progress func(int64)) error {
	if cli, err := bkp.Download(ctx, &backup.BackupBlob{Type: btype}); err != nil {
		return err
	} else {
		lastProgress := time.Unix(0, 0)
		cnt := int64(0)
		for ctx.Err() == nil {
			if item, err := cli.Recv(); err != nil {
				if err == io.EOF {
					break
				}
				return err
			} else {
				cnt++
				if blob, err := Marshal(item); err != nil {
					return err
				} else {
					now := time.Now()
					if now.Sub(lastProgress) > 500*time.Millisecond {
						lastProgress = now
						if progress != nil {
							progress(cnt)
						}
					}
					size := int32(len(blob))
					if err := binary.Write(writer, binary.BigEndian, size); err != nil {
						return err
					}
					if err := binary.Write(writer, binary.BigEndian, blob); err != nil {
						return err
					}
				}
			}
		}
		if progress != nil {
			progress(cnt)
		}
		return ctx.Err()
	}
}

type Task struct {
	Payload          interface{}
	Id               string
	Type             string
	Queue            string
	CallbackEndpoint string
	When             time.Time
	IfNotExists      bool
}

func (t *Tasks) Schedule(ctx context.Context, in *Task, opts ...grpc.CallOption) (*Task, error) {
	task := &tasks.Task{
		Id:               in.Id,
		Queue:            in.Queue,
		CallbackEndpoint: in.CallbackEndpoint,
		IfNotExists:      in.IfNotExists,
	}
	if !in.When.IsZero() {
		task.When = timestamppb.New(in.When)
	}
	if in.Payload != nil {
		if payload, err := Marshal(in.Payload); err != nil {
			return nil, err
		} else {
			task.Payload = payload
			task.PayloadType = PayloadName(in.Payload)
		}
	}
	metrics.ContextStats(ctx).IncWrites()
	if tsk, e := t.OperatorClient.Schedule(ctx, task, opts...); e != nil {
		return nil, e
	} else {
		in.Id = tsk.Id
		in.When = tsk.When.AsTime()
	}
	return in, nil
}
func (t *Tasks) Cancel(ctx context.Context, in *Task, opts ...grpc.CallOption) (*tasks.Task, error) {
	metrics.ContextStats(ctx).IncWrites()
	return t.OperatorClient.Cancel(ctx, &tasks.Task{Id: in.Id, Queue: in.Queue}, opts...)
}

type JmotClient struct {
	connection *grpc.ClientConn
	Journals   *Journals
	Arrays     *Arrays
	Queues     *Queues
	Tasks      *Tasks
	Backup     *Backups
}

func (jc *JmotClient) InitKeeperFromConnection(connection *grpc.ClientConn) error {
	if connection != nil {
		jc.Journals = &Journals{journals.NewOperatorClient(connection)}
		jc.Arrays = &Arrays{arrays.NewOperatorClient(connection)}
		jc.Queues = &Queues{queues.NewOperatorClient(connection)}
		jc.Backup = &Backups{backup.NewBackupClient(connection)}
	}
	return nil
}

func (jc *JmotClient) InitTasksFromConnection(connection *grpc.ClientConn) error {
	if connection != nil {
		jc.Tasks = &Tasks{tasks.NewOperatorClient(connection)}
	}
	return nil
}

var pool, _ = x509.SystemCertPool()

var insecureTransportCreds = credentials.NewTLS(&tls.Config{
	InsecureSkipVerify: true,
	RootCAs:            pool,
})

var secureTransportCreds = credentials.NewTLS(&tls.Config{
	InsecureSkipVerify: false,
	RootCAs:            pool,
})

func WithTlsVerifyOptions(verify bool, opts []grpc.DialOption) []grpc.DialOption {
	if verify {
		opts = append(opts, grpc.WithTransportCredentials(secureTransportCreds))
	} else {
		opts = append(opts, grpc.WithTransportCredentials(insecureTransportCreds))
	}
	return opts
}
func WithTokenAuthOptions(opts []grpc.DialOption) []grpc.DialOption {
	opts = append(opts, grpc.WithPerRPCCredentials(&middleware.TokenAuth{Token: *jmotApiKey, RequireTLS: true}))
	return opts
}

func WithFlagsOptions(opts []grpc.DialOption) []grpc.DialOption {
	fopts := WithTlsVerifyOptions(*jmotSecure, nil)
	if *jmotApiKey != "" {
		fopts = append(fopts, grpc.WithPerRPCCredentials(&middleware.TokenAuth{Token: *jmotApiKey, RequireTLS: true}))
	}
	if *waitForReady {
		fopts = append(fopts, grpc.WithDefaultCallOptions(grpc.WaitForReady(true)))
	}
	return append(fopts, opts...)
}

func (jc *JmotClient) InitTasksClient(endpoint string, opts ...grpc.DialOption) error {
	if c, err := grpc.Dial(endpoint, WithFlagsOptions(opts)...); err != nil {
		return err
	} else {
		return jc.InitTasksFromConnection(c)
	}
}

func (jc *JmotClient) InitKeeperClient(endpoint string, opts ...grpc.DialOption) error {
	if c, err := grpc.Dial(endpoint, WithFlagsOptions(opts)...); err != nil {
		return err
	} else {
		return jc.InitKeeperFromConnection(c)
	}
}

func NewDefaultClient(opts ...grpc.DialOption) (*JmotClient, error) {
	logging.InitLogger()
	client := new(JmotClient)
	if err := client.InitTasksClient(*jmotTasksServer, opts...); err != nil {
		return nil, err
	}
	if err := client.InitKeeperClient(*jmotKeeperServer, opts...); err != nil {
		return nil, err
	}
	return client, nil
}
