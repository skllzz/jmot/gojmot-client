// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.18.1
// source: queues/queues_service.proto

package queues

import (
	context "context"
	data "gitlab.com/skllzz/jmot/gojmot-client/pb/keeper/data"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// OperatorClient is the client API for Operator service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type OperatorClient interface {
	// чтение элементов из очереди в порядке pull_millis и с условиями диапазона PullRequest.range
	Pull(ctx context.Context, in *PullRequest, opts ...grpc.CallOption) (Operator_PullClient, error)
	// добавление или обновление данных в очереди, возвращает прошлое значение элемента
	Push(ctx context.Context, in *PushRequest, opts ...grpc.CallOption) (*data.Item, error)
	// удаляет элемент из очереди (если такого элемента нет то вернется ошибка)
	Delete(ctx context.Context, in *DeleteRequest, opts ...grpc.CallOption) (*data.Item, error)
}

type operatorClient struct {
	cc grpc.ClientConnInterface
}

func NewOperatorClient(cc grpc.ClientConnInterface) OperatorClient {
	return &operatorClient{cc}
}

func (c *operatorClient) Pull(ctx context.Context, in *PullRequest, opts ...grpc.CallOption) (Operator_PullClient, error) {
	stream, err := c.cc.NewStream(ctx, &Operator_ServiceDesc.Streams[0], "/queues.Operator/Pull", opts...)
	if err != nil {
		return nil, err
	}
	x := &operatorPullClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type Operator_PullClient interface {
	Recv() (*data.Item, error)
	grpc.ClientStream
}

type operatorPullClient struct {
	grpc.ClientStream
}

func (x *operatorPullClient) Recv() (*data.Item, error) {
	m := new(data.Item)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *operatorClient) Push(ctx context.Context, in *PushRequest, opts ...grpc.CallOption) (*data.Item, error) {
	out := new(data.Item)
	err := c.cc.Invoke(ctx, "/queues.Operator/Push", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *operatorClient) Delete(ctx context.Context, in *DeleteRequest, opts ...grpc.CallOption) (*data.Item, error) {
	out := new(data.Item)
	err := c.cc.Invoke(ctx, "/queues.Operator/Delete", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// OperatorServer is the server API for Operator service.
// All implementations should embed UnimplementedOperatorServer
// for forward compatibility
type OperatorServer interface {
	// чтение элементов из очереди в порядке pull_millis и с условиями диапазона PullRequest.range
	Pull(*PullRequest, Operator_PullServer) error
	// добавление или обновление данных в очереди, возвращает прошлое значение элемента
	Push(context.Context, *PushRequest) (*data.Item, error)
	// удаляет элемент из очереди (если такого элемента нет то вернется ошибка)
	Delete(context.Context, *DeleteRequest) (*data.Item, error)
}

// UnimplementedOperatorServer should be embedded to have forward compatible implementations.
type UnimplementedOperatorServer struct {
}

func (UnimplementedOperatorServer) Pull(*PullRequest, Operator_PullServer) error {
	return status.Errorf(codes.Unimplemented, "method Pull not implemented")
}
func (UnimplementedOperatorServer) Push(context.Context, *PushRequest) (*data.Item, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Push not implemented")
}
func (UnimplementedOperatorServer) Delete(context.Context, *DeleteRequest) (*data.Item, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}

// UnsafeOperatorServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to OperatorServer will
// result in compilation errors.
type UnsafeOperatorServer interface {
	mustEmbedUnimplementedOperatorServer()
}

func RegisterOperatorServer(s grpc.ServiceRegistrar, srv OperatorServer) {
	s.RegisterService(&Operator_ServiceDesc, srv)
}

func _Operator_Pull_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(PullRequest)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(OperatorServer).Pull(m, &operatorPullServer{stream})
}

type Operator_PullServer interface {
	Send(*data.Item) error
	grpc.ServerStream
}

type operatorPullServer struct {
	grpc.ServerStream
}

func (x *operatorPullServer) Send(m *data.Item) error {
	return x.ServerStream.SendMsg(m)
}

func _Operator_Push_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(PushRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OperatorServer).Push(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/queues.Operator/Push",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OperatorServer).Push(ctx, req.(*PushRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Operator_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OperatorServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/queues.Operator/Delete",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OperatorServer).Delete(ctx, req.(*DeleteRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// Operator_ServiceDesc is the grpc.ServiceDesc for Operator service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var Operator_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "queues.Operator",
	HandlerType: (*OperatorServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Push",
			Handler:    _Operator_Push_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _Operator_Delete_Handler,
		},
	},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "Pull",
			Handler:       _Operator_Pull_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "queues/queues_service.proto",
}
