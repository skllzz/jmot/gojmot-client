package gojmot_client

import (
	"context"
	"gitlab.com/skllzz/jmot/gojmot-client/metrics"
	"gitlab.com/skllzz/jmot/gojmot-client/pb/keeper/data"
	"google.golang.org/grpc"
	"io"
)

type ItemReader interface {
	Recv() (*data.Item, error)
	grpc.ClientStream
}

type Item struct {
	*data.Item
	streamError error
}

func (v *Item) IsError() error {
	return v.streamError
}

type contextReader struct {
	ctx context.Context
}

func reader(ctx context.Context) *contextReader {
	if ctx == nil {
		ctx = context.Background()
	}
	return &contextReader{ctx: ctx}
}

func (cr *contextReader) dataOrErrorChannel(reader ItemReader, err error) <-chan *Item {
	res := make(chan *Item, 10)
	stats := metrics.ContextStats(cr.ctx)
	stats.IncReads()
	go func() {
		defer close(res)
		var item *data.Item
		for cr.ctx.Err() == nil {
			if err != nil || item != nil {
				select {
				case <-cr.ctx.Done():
					return
				case res <- &Item{streamError: err, Item: item}:
					if err != nil {
						return
					}
				}
			}
			item, err = reader.Recv()
			if err == io.EOF {
				break
			}
			if err == nil {
				stats.IncReads()
			}
		}
	}()
	return res
}
