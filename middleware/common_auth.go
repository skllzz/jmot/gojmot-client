package middleware

import (
	"context"
	"google.golang.org/grpc"
)

type ContextKey string

func (c ContextKey) String() string {
	return "Context key " + string(c)
}

type WrappedServerStream struct {
	grpc.ServerStream
	WrappedContext context.Context
}

func (w *WrappedServerStream) Context() context.Context {
	return w.WrappedContext
}
