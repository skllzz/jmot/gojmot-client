package middleware

import (
	"context"
	"fmt"
	"gitlab.com/skllzz/jmot/gojmot-client/logging"
	"gitlab.com/skllzz/jmot/gojmot-client/metrics"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"os"
)

var contextTokenAuth = ContextKey("token-auth")
var staticToken = ""

type TokenAuth struct {
	Token      string
	RequireTLS bool
}

func (t *TokenAuth) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	res := make(map[string]string)
	res["TOKEN"] = t.Token
	return res, nil
}

func (t *TokenAuth) RequireTransportSecurity() bool {
	return t.RequireTLS
}
func init() {
	if value := os.Getenv("API_KEY"); value != "" {
		staticToken = value
	}
}

func GetStaticAuthToken(ctx context.Context) (string, error) {
	tokenContext, err := tokenAuthContext(ctx)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%v", tokenContext.Value(contextTokenAuth)), err
}

func tokenAuthContext(ctx context.Context) (context.Context, error) {
	md, _ := metadata.FromIncomingContext(ctx)
	tokens := md.Get("TOKEN")
	var token string
	if len(tokens) > 0 {
		token = tokens[0]
	}
	if token != staticToken {
		return nil, status.Error(codes.Unauthenticated, "auth token is invalid")
	}
	logging.Default.Debug("GRPC auth", zap.String("token", token))
	return context.WithValue(ctx, contextTokenAuth, token), nil
}

func CheckUnaryTokenAuth(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	metrics.ApiCalls.WithLabelValues(info.FullMethod).Inc()
	logging.Default.WithOptions(zap.AddCallerSkip(1)).Debug("Call unary method", zap.String("method", info.FullMethod))
	if nctx, err := tokenAuthContext(ctx); err != nil {
		metrics.ApiErrors.WithLabelValues(info.FullMethod).Inc()
		logging.Default.WithOptions(zap.AddCallerSkip(1)).Info("Method auth error", zap.String("method", info.FullMethod), zap.Error(err))
		return nil, err
	} else {
		resp, err := handler(nctx, req)
		if err != nil && err != context.Canceled && ctx.Err() == nil {
			metrics.ApiErrors.WithLabelValues(info.FullMethod).Inc()
			logging.Default.WithOptions(zap.AddCallerSkip(1)).Warn("Method return with error", zap.String("method", info.FullMethod), zap.Error(err))
		}
		return resp, err
	}
}

func CheckStreamTokenAuth(srv interface{}, ss grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
	metrics.ApiCalls.WithLabelValues(info.FullMethod).Inc()
	logging.Default.WithOptions(zap.AddCallerSkip(1)).Debug("Call stream method", zap.String("method", info.FullMethod))
	if ctx, err := tokenAuthContext(ss.Context()); err != nil {
		metrics.ApiErrors.WithLabelValues(info.FullMethod).Inc()
		logging.Default.WithOptions(zap.AddCallerSkip(1)).Warn("Method auth error", zap.String("method", info.FullMethod), zap.Error(err))
		return err
	} else {
		err = handler(srv, &WrappedServerStream{ServerStream: ss, WrappedContext: ctx})
		if err != nil && err != context.Canceled && ctx.Err() == nil {
			metrics.ApiErrors.WithLabelValues(info.FullMethod).Inc()
			logging.Default.WithOptions(zap.AddCallerSkip(1)).Warn("Method return with error", zap.String("method", info.FullMethod), zap.Error(err))
		}
		return err
	}
}
