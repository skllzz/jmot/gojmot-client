package main

import (
	"compress/flate"
	"compress/zlib"
	"context"
	"flag"
	"fmt"
	gojmot_client "gitlab.com/skllzz/jmot/gojmot-client"
	"gitlab.com/skllzz/jmot/gojmot-client/logging"
	"gitlab.com/skllzz/jmot/gojmot-client/pb/keeper/backup"
	"go.uber.org/zap"
	"os"
	"time"
)

var backupFile = flag.String("backup-file", "", "backup file")
var restoreFile = flag.String("restore-file", "", "restore file")

func main() {
	flag.Parse()
	logging.InitLogger()
	ctx := context.Background()
	logging.Default.Info("JMOT backup/restore")
	if client, cerr := gojmot_client.NewDefaultClient(); cerr != nil {
		logging.Default.Fatal("unable to create jmot client", zap.Error(cerr))
	} else {
		if *backupFile != "" {
			logging.Default.Info("Creating backup", zap.String("file", *backupFile))
			if out, ferr := os.Create(*backupFile); ferr != nil {
				logging.Default.Fatal("unable to create backup file", zap.Error(ferr))
			} else {
				if wr, err := zlib.NewWriterLevel(out, flate.BestCompression); err != nil {
					logging.Default.Fatal("unable to create compression layer", zap.Error(err))
				} else {
					lastProgress := int64(0)
					if err := client.Backup.WriteTo(ctx, backup.BackupBlob_unknown, wr, func(i int64) {
						fmt.Printf("\rProgress: %v  ", i)
						lastProgress = i
					}); err != nil {
						fmt.Println()
						logging.Default.Fatal("unable to write backup file", zap.Error(err))
					} else {
						fmt.Println()
						if err := wr.Close(); err != nil {
							logging.Default.Fatal("unable to write backup file", zap.Error(err))
						}
						if err := out.Close(); err != nil {
							logging.Default.Fatal("unable to write backup file", zap.Error(err))
						}
						logging.Default.Info("backup success", zap.Int64("record_count", lastProgress))
					}
				}
			}
		}
		if *restoreFile != "" {
			logging.Default.Info("Loading backup", zap.String("file", *restoreFile))
			if in, ferr := os.Open(*restoreFile); ferr != nil {
				logging.Default.Fatal("unable to open backup file", zap.Error(ferr))
			} else {
				if rd, zerr := zlib.NewReader(in); ferr != nil {
					logging.Default.Fatal("unable to read compression layer", zap.Error(zerr))
				} else {
					cnt := int64(0)
					if uploader, uerr := client.Backup.Upload(ctx); uerr != nil {
						logging.Default.Fatal("unable to connect jmot uploader", zap.Error(uerr))
					} else {
						lastProgress := time.Unix(0, 0)
						if err := client.Backup.ParseReader(ctx, rd, func(ctx context.Context, blob *backup.BackupBlob) error {
							cnt++
							now := time.Now()
							if now.Sub(lastProgress) > 500*time.Millisecond {
								lastProgress = now
								fmt.Printf("\rProgress: %v  ", cnt)
							}
							return uploader.Send(blob)
						}); err != nil {
							fmt.Println()
							logging.Default.Fatal("unable to read backup", zap.Error(err))
						} else {
							fmt.Printf("\rProgress: %v\n", cnt)
						}
						if err := rd.Close(); err != nil {
							logging.Default.Fatal("unable to read backup file", zap.Error(err))
						}
						if err := in.Close(); err != nil {
							logging.Default.Fatal("unable to read backup file", zap.Error(err))
						}
						logging.Default.Info("Wait to complete...")
						if _, err := uploader.CloseAndRecv(); err != nil {
							logging.Default.Fatal("unable to upload backup file", zap.Error(err))
						} else {
							logging.Default.Info("restore success", zap.Int64("record_count", cnt))
						}

					}
				}
			}

		}
	}
}
