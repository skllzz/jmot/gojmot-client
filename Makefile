.PHONY: install
install:
	go install gitlab.com/skllzz/jmot/gojmot-client/cmd/jmot-backup

.PHONY: init
init:
	#sudo apt install -y protobuf-compiler
	go install github.com/golang/protobuf/protoc-gen-go@latest

.PHONY: proto
proto: # clone or symlink git@gitlab.com:skllzz/jmot/api/keeper.git to proto/keeper folder
	rm -rf pb
	mkdir -p pb/keeper
	protoc -I proto/keeper --openapiv2_out pb \
                    --openapiv2_opt logtostderr=true \
                    --openapiv2_opt allow_merge=true \
                    --openapiv2_opt generate_unbound_methods=true \
                    --grpc-gateway_out=pb/keeper  --grpc-gateway_opt paths=source_relative --grpc-gateway_opt logtostderr=true --grpc-gateway_opt generate_unbound_methods=true  \
                    --go-grpc_out=pb/keeper --go-grpc_opt=require_unimplemented_servers=false,paths=source_relative \
                    --go_out=pb/keeper --go_opt=paths=source_relative \
                    $(shell find -L proto/keeper -type f -name '*.proto' -printf '%P\n')
	
.PHONY: build
build:
	go build -o helloworld *.go


.PHONY: test
test:
	go test -v ./... -cover

.PHONY: update
update:
	go get -v -u ./...

.PHONY: tls
tls:
	mkdir -p tls
	openssl req -x509 -nodes -days 36500 -newkey rsa:2048 -subj "/CN=jmot.client" -keyout tls/server.key -out tls/server.crt

.PHONY: nats
nats:
	docker run --name nats --network cloudbuild -d --rm -p 4222:4222 -p 8181:8181 -p 8180:8180 --mount type=bind,source=${CURDIR}/nats.conf,target=/config/nats.conf nats --config /config/nats.conf

.PHONY: scylla
scylla:
	docker run --name scylla --network cloudbuild --rm -d -p 8000:8000 scylladb/scylla --smp 1 --alternator-port=8000 --alternator-write-isolation=always

.PHONY: locker
locker:
	docker run --name locker --network cloudbuild --rm -d -p 8443:8443 softkot/locker

.PHONY: logs
logs:
	docker logs scylla
	docker logs locker
	docker logs nats


.PHONY: cbuild
cbuild:
	gcloud builds submit --project balamutbot --substitutions=_COUNTER_KEY=journaldb,COMMIT_SHA=ef070d453ca7fe512f43a,BRANCH_NAME=master,SHORT_SHA=ef070d453ca7fe512f43a


.PHONY: test-ydb # требует корректных .aws/credentials и .aws/config
test-ydb:
	go test -v -c jmot-keeper/tests -o keeper.test
	./keeper.test --test.v --namespace deploy --ddb-name https://docapi.serverless.yandexcloud.net/ru-central1/b1g8c0183nvcco5a7qaq/etn010h3dv7rcqs2tb3i

.PHONY: test-local # требует корректных .aws/credentials и .aws/config
test-local:
	go test -v -c jmot-keeper/tests -o keeper.test
	./keeper.test --test.v --namespace deploy
