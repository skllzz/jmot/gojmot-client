package gojmot_client

import (
	"context"
	"crypto/tls"
	_ "embed"
	"fmt"
	"gitlab.com/skllzz/jmot/gojmot-client/logging"
	"gitlab.com/skllzz/jmot/gojmot-client/pb/keeper/tasks"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/keepalive"
	"google.golang.org/protobuf/types/known/emptypb"
	"net"
	"strconv"
	"strings"
	"time"
)

var _ tasks.CallbackServer = (*taskCallbackServer)(nil)

type TaskCallbackFunc func(ctx context.Context, task *tasks.Task) error

type taskCallbackServer struct {
	callback TaskCallbackFunc
}

type TaskCallbackInstance struct {
	endpoint string
	server   *taskCallbackServer
	local    net.Addr
	grpc     *grpc.Server
	listner  net.Listener
}

func (t *taskCallbackServer) Run(ctx context.Context, task *tasks.Task) (*emptypb.Empty, error) {

	if err := t.callback(ctx, task); err != nil {
		return nil, err
	}
	return new(emptypb.Empty), nil
}

func (t *TaskCallbackInstance) Shutdown() error {
	t.grpc.GracefulStop()
	t.listner.Close()
	return nil
}

func (t *TaskCallbackInstance) CreateTask(queueName, id string, when time.Time, payload interface{}) *Task {
	return &Task{
		Queue:            queueName,
		Id:               id,
		CallbackEndpoint: t.Endpoint(),
		Payload:          payload,
		When:             when,
	}
}

func (t *TaskCallbackInstance) CreateTaskIfNotExists(queueName, id string, when time.Time, payload interface{}) *Task {
	return &Task{
		Queue:            queueName,
		Id:               id,
		CallbackEndpoint: t.Endpoint(),
		Payload:          payload,
		When:             when,
		IfNotExists:      true,
	}
}

func (t *TaskCallbackInstance) Endpoint() string {
	return t.endpoint
}

//go:embed tls/server.crt
var tlsCert []byte

//go:embed tls/server.key
var tlsKey []byte

var cert, _ = tls.X509KeyPair(tlsCert, tlsKey)
var tlsConfig = &tls.Config{
	InsecureSkipVerify:       true,
	Certificates:             []tls.Certificate{cert},
	PreferServerCipherSuites: true,
	RootCAs:                  pool,
	CurvePreferences: []tls.CurveID{
		tls.CurveP256,
		tls.X25519,
	},
}

func NewDefaultTaskCallback(ctx context.Context, callback TaskCallbackFunc) (*TaskCallbackInstance, error) {
	return NewTaskCallback(ctx, "", callback)
}
func NewTaskCallback(ctx context.Context, hostname string, callback TaskCallbackFunc) (*TaskCallbackInstance, error) {
	grpcServer := grpc.NewServer(
		grpc.Creds(credentials.NewTLS(tlsConfig)),
		grpc.ConnectionTimeout(10*time.Second),
		grpc.WriteBufferSize(16*1026),
		grpc.MaxRecvMsgSize(16*1024*1024),
		grpc.MaxSendMsgSize(16*1024*1024),
		grpc.MaxConcurrentStreams(1024),
		grpc.NumStreamWorkers(512),
		grpc.InitialWindowSize(64*1024),
		grpc.InitialConnWindowSize(64*1024),
		grpc.KeepaliveParams(keepalive.ServerParameters{
			Time:    30 * time.Second,
			Timeout: 10 * time.Second,
		}),
	)
	instance := new(TaskCallbackInstance)
	if hostname == "" {
		hostname = *taskCallbackHostName
	}
	parts := strings.Split(hostname, ":")
	port := 0
	if len(parts) > 0 {
		hostname = strings.TrimSpace(parts[0])
	}
	if len(parts) > 1 {
		port, _ = strconv.Atoi(parts[1])
	}
	instance.server = &taskCallbackServer{callback: callback}
	tasks.RegisterCallbackServer(grpcServer, instance.server)
	if l, err := net.Listen("tcp", fmt.Sprintf(":%v", port)); err != nil {
		return nil, err
	} else {
		instance.listner = l
		instance.local = l.Addr() //.(*net.TCPAddr).Port
		if hostname == "" {
			hostname = "localhost"
		}
		instance.endpoint = fmt.Sprintf("%v:%v", hostname, l.Addr().(*net.TCPAddr).Port)
		instance.grpc = grpcServer
		go func() {
			logging.Default.Info("callback server listening",
				zap.Any("addr", l.Addr()),
				zap.Any("endpoint", instance.endpoint),
			)
			if err := grpcServer.Serve(l); err != nil {
				logging.Default.Warn("callback server finished", zap.Error(err))
			}
		}()
		go func() {
			<-ctx.Done()
			_ = instance.Shutdown()
		}()
	}
	return instance, nil
}
