package tests

import (
	"context"
	"flag"
	gojmot_client "gitlab.com/skllzz/jmot/gojmot-client"
	"gitlab.com/skllzz/jmot/gojmot-client/logging"
	"gitlab.com/skllzz/jmot/gojmot-client/pb/keeper/arrays"
	"gitlab.com/skllzz/jmot/gojmot-client/pb/keeper/tasks"
	"go.uber.org/zap"
	"google.golang.org/protobuf/types/known/timestamppb"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"testing"
	"time"
)

var jmot *gojmot_client.JmotClient
var ctx context.Context

// to get pod ip use
// kubectl -n master get service jmot-keeper
func init() {
	ctx, _ = signal.NotifyContext(context.Background(), os.Interrupt, os.Kill, syscall.SIGINT, syscall.SIGTERM)
	testing.Init()
	flag.Parse()
	logging.InitLogger()
	if jc, err := gojmot_client.NewDefaultClient(); err != nil {
		panic(err)
	} else {
		jmot = jc
	}
}

func TestConnection(t *testing.T) {
	if v, err := jmot.Arrays.Value(ctx, &arrays.ValueRequest{Id: "qwe", ArrayName: "test"}); err != nil {
		t.Fatal(err)
	} else {
		t.Log(v)
	}
}

func TestTasks(t *testing.T) {
	wg := new(sync.WaitGroup)
	if cb, err := gojmot_client.NewTaskCallback(ctx, "172.24.213.168:6542", func(ctx context.Context, task *tasks.Task) error {
		logging.Default.Info("task ok", zap.Any("task", task))
		wg.Done()
		//return errors.New("reshed")
		return nil
	}); err != nil {
		t.Fatal(err)
	} else {
		wg.Add(1)
		if true {
			if _, err := jmot.Tasks.Schedule(ctx,
				cb.CreateTask("start", "id", time.Now().Add(0*time.Second), timestamppb.New(time.Now()))); err != nil {
				t.Fatal(err)
			} else {
				//jmot.Tasks.Cancel(ctx, rtask)
			}
		}
		wg.Wait()
		if err := cb.Shutdown(); err != nil {
			t.Fatal(err)
		}
		<-time.After(1 * time.Second)

	}
}
