package logging

import (
	"flag"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"sync"
)

var Debug = flag.Bool("debug", false, "turn on debug messages")
var Json = flag.Bool("json-log", false, "Log to json")
var Default *zap.Logger
var WithoutStackTrace *zap.Logger

type neverLevel struct {
}

var Never = &neverLevel{}

func (n neverLevel) Enabled(level zapcore.Level) bool {
	return false
}

var _ zapcore.LevelEnabler = (*neverLevel)(nil)

func Mask(s string) string {
	ns := ""
	ls := len(s)
	mpos := ls / 3
	lpos := ls * 2 / 3
	if mpos > 8 {
		mpos = 8
	}
	if lpos < ls-8 {
		lpos = ls - 8
	}
	for i, r := range s {
		if i >= mpos && i <= lpos {
			ns = ns + "*"
		} else {
			ns = ns + string(r)
		}
	}
	return ns
}

var loggerOnce = new(sync.Once)

func InitLogger() {
	loggerOnce.Do(func() {

		config := zap.NewDevelopmentConfig()
		if *Json {
			config = zap.NewProductionConfig()
		}
		if *Debug {
			config.Level = zap.NewAtomicLevelAt(zap.DebugLevel)
		} else {
			config.Level = zap.NewAtomicLevelAt(zap.InfoLevel)
		}
		config.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
		if *Json {
			config.EncoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
		}
		Default, _ = config.Build(zap.AddCaller(), zap.Development(), zap.AddStacktrace(zapcore.ErrorLevel))
		WithoutStackTrace = Default.WithOptions(zap.AddStacktrace(&neverLevel{}))
	})
	// Should only be done from init functions
}
