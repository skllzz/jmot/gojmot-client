package gojmot_client

import (
	"flag"
	"os"
)

var jmotKeeperServer = flag.String("jmot-keeper-server", "localhost:7000", "JMOT keeper GRPC TLS endpoint")
var jmotTasksServer = flag.String("jmot-tasks-server", "localhost:7001", "JMOT tasks GRPC TLS endpoint")
var jmotSecure = flag.Bool("jmot-secure", false, "strict tls verify")
var jmotApiKey = flag.String("jmot-key", os.Getenv("API_KEY"), "API key (overrige skip API_KEY env var")
var waitForReady = flag.Bool("wait-for-ready", false, "Control WaitForReady GRPC connect option ")
var taskCallbackHostName = flag.String("task-callback-hostname", "localhost:0", "hostname for callback calls")
